type Texto = [Char]

-- 1.a
type Punto2D = (Float, Float)
prodInt :: Punto2D -> Punto2D -> Float
prodInt (ax, ay) (bx, by) = ax * bx + ay * by

-- 1.b
todoMenor :: Punto2D -> Punto2D -> Bool
todoMenor (ax, ay) (bx, by) = ax < bx && ay < by

-- 1.c
distanciaPuntos :: Punto2D -> Punto2D -> Float
distanciaPuntos (ax, ay) (bx, by) = sqrt sumaCuadrados
    where distX = abs (ax - bx)
          distY = abs (ay - by)
          sumaCuadrados = (distX ^ 2) + (distY ^ 2)

-- 1.d
type Coordenada = (Float, Float)
crearPar :: Float -> Float -> Coordenada
crearPar x y = (x, y)

-- 2
type Año = Integer
type EsBisiesto = Bool

-- ridiculo usar types de este estilo, me recuerda a los types que uso en rust para asegurar una interfaz estable con scylladb, solo que esos types sirven de algo y estos no
bisiesto :: Año -> EsBisiesto
bisiesto año = not noEsBisiesto
    where noEsBisiesto = noEsMultiploDeCuatro || (esMultiploDeCien && noEsMultiploDe400)
          noEsMultiploDeCuatro = año `mod` 4 /= 0
          esMultiploDeCien = año `mod` 100 == 0
          noEsMultiploDe400 = año `mod` 400 /= 0

-- 3
type Coordenada3D = (Float, Float, Float)
distanciaManhattan :: Coordenada3D -> Coordenada3D -> Float
distanciaManhattan (ax, ay, az) (bx, by, bz) = abs (ax - bx) + abs (ay - by) + abs (az - bz)

-- 4
type Nombre = Texto
type Telefono = Texto
type Contacto = (Nombre, Telefono)
type ContactosTel = [Contacto]

-- 4.a
enLosContactos :: Nombre -> ContactosTel -> Bool
enLosContactos _ [] = False
enLosContactos v [(a, _)] = v == a
enLosContactos v ((contacto, _):xs) | v == contacto = True
                                    | otherwise = enLosContactos v xs

-- 4.b
agregarContacto :: Contacto -> ContactosTel -> ContactosTel
agregarContacto contacto [] = [contacto]
agregarContacto (conNom, conTel) [(nom, tel)] | conNom == nom = [(conNom, conTel)]
                                              | otherwise = (nom, tel) : [(conNom, conTel)]
agregarContacto con (x:xs) | fst x == fst con = con:agregarContacto con xs
                           | otherwise = x:agregarContacto con xs

-- 4.c
eliminarContacto :: Nombre -> ContactosTel -> ContactosTel
eliminarContacto _ [] = []
eliminarContacto cNom [cnt] | cNom == fst cnt = []
                            | otherwise = [cnt]
eliminarContacto cNom (x:xs) | fst x == cNom = eliminarContacto cNom xs
                             | otherwise = x:eliminarContacto cNom xs

-- 5
type Identificacion = Integer
type Ubicacion = Texto
type Estado = (Disponibilidad, Ubicacion)
type Locker = (Identificacion, Estado)
type MapaDeLockers = [Locker]
data Disponibilidad = Libre | Ocupado deriving (Eq, Show)

-- 5.1
existeElLocker :: Identificacion -> MapaDeLockers -> Bool
existeElLocker id [] = False
existeElLocker id [(vId, _)] = id == vId
existeElLocker id (x:xs) | id == fst x = True
                         | otherwise = existeElLocker id xs

-- 5.2, el locker EXISTE
ubicacionDelLocker :: Identificacion -> MapaDeLockers -> Ubicacion
ubicacionDelLocker id [(vId, (_, vUb))] | id == vId = vUb
ubicacionDelLocker id (x:xs) | id == fst x = snd (snd x)
                             | otherwise = ubicacionDelLocker id xs

-- 5.3, el locker EXISTE
estaDisponibleElLocker :: Identificacion -> MapaDeLockers -> Bool
estaDisponibleElLocker id [(vId, (vDis, _))] | id == vId = vDis == Libre
estaDisponibleElLocker id (x:xs) | id == fst x = fst (snd x) == Libre
                                 | otherwise = estaDisponibleElLocker id xs

-- 5.4, el locker EXISTE y está LIBRE (en caso contrario da error)
ocuparLocker :: Identificacion -> MapaDeLockers -> MapaDeLockers
ocuparLocker id [(vId, (vDis, vUbic))] | vId == id && vDis == Libre = [(vId, (Ocupado, vUbic))]
ocuparLocker id (x:xs) | id == fst x && estaLibre = (id, (Ocupado, snd (snd x))) : xs
                       | id == fst x && not estaLibre = undefined
                       | otherwise = x : ocuparLocker id xs
                       where estaLibre = fst (snd x) == Libre

main = do

    print (enLosContactos "Juan" [("Pepe", "19821789127"), ("Juan", "897123")]) -- True
    print (enLosContactos "Felipe" [("Pepe", "19821789127"), ("Juan", "897123")]) -- False

    print (agregarContacto ("pepe", "87127892") []) -- [("pepe", "87127892")]
    print (agregarContacto ("pepe", "87127892") [("pepe", "1")]) -- [("pepe", "87127892")]
    print (agregarContacto ("pepe", "87127892") [("alberto", "09123091")]) -- [("alberto", "09123091"), ("pepe", "87127892")]
    print (agregarContacto ("pepe", "87127892") [("alberto", "09123091"), ("pepe", "1")]) -- [("alberto", "09123091"), ("pepe", "87127892")]

    print (eliminarContacto "juan" [("pepe", "1982789"), ("juan", "17892"), ("felipe", "189218")])

    let lockers = [(100,(Ocupado,"ZD39I")), (101,(Libre,"JAH3I")), (103,(Libre,"IQSA9")), (105,(Libre,"QOTSA")), (109,(Ocupado,"893JJ")), (110,(Ocupado,"99292"))]
    
    print (existeElLocker 100 lockers) -- True
    print (existeElLocker 54 lockers) -- False
    print (ubicacionDelLocker 101 lockers) -- "JAH3I"
    print (estaDisponibleElLocker 100 lockers) -- False
    print (estaDisponibleElLocker 101 lockers) -- True
    print (ocuparLocker 101 lockers) -- lo mismo pero con el 101 Ocupado

-- de todas formas hay que ser bastante salame como para elegir haskell para implementar algo del estilo
