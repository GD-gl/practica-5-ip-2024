import Text.XHtml (rev)
-- 1.1
longitud :: [t] -> Int
longitud [] = 0
longitud (x:xs) = 1 + longitud xs

-- 1.2
-- longitud > 0
ultimo :: [t] -> t
ultimo [u] = u
ultimo (x:xs) = ultimo xs

-- 1.3
-- longitud > 0
principio :: [t] -> [t]
principio [t] = [] -- saca el ultimo
principio (x:xs) = x:principio xs

-- 1.4
reverso :: [t] -> [t]
reverso [] = []
reverso (x:xs) = reverso xs ++ [x]

-- 2.1
pertenece :: (Eq t) => t -> [t] -> Bool
pertenece _ [] = False
pertenece valor [elemento] = valor == elemento
pertenece valor (e:es) = e == valor || pertenece valor es

-- 2.2
todosIguales :: (Eq t) => [t] -> Bool
todosIguales [] = True
todosIguales [x] = True
todosIguales (x:y:xs) = x == y && todosIguales (y:xs)

-- 2.3
todosDistintos :: (Eq t) => [t] -> Bool
todosDistintos [] = True
todosDistintos (x:xs) = not(pertenece x xs) && todosDistintos xs

-- 2.4
hayRepetidos :: (Eq t) => [t] -> Bool
hayRepetidos [] = False
hayRepetidos (x:xs) = x `pertenece` xs || hayRepetidos xs

-- 2.5
quitar :: (Eq t) => t -> [t] -> [t]
quitar _ [] = []
quitar valor (x:xs) | valor == x = xs -- saco x, devuelvo el resto
                    | otherwise = x : quitar valor xs -- sigo

-- 2.6
quitarTodos :: (Eq t) => t -> [t] -> [t]
quitarTodos _ [] = []
quitarTodos valor (x:xs) | valor == x = quitarTodos valor xs
                         | otherwise = x : quitarTodos valor xs

-- 2.7
eliminarRepetidos :: (Eq t) => [t] -> [t]
eliminarRepetidos [] = []
eliminarRepetidos (x:xs) | x `pertenece` xs = x : eliminarRepetidos (quitarTodos x xs)
                         | otherwise = x : eliminarRepetidos xs

-- 2.8
-- Esta función sólo se fija si todo elemento de s pertenece a r (no es 'sí y solo sí')
mismosElementosInterno :: (Eq t) => [t] -> [t] -> Bool
mismosElementosInterno [] _ = True
mismosElementosInterno (x:xs) ys = (x `pertenece` ys) && mismosElementosInterno xs ys

-- Esta función se asegura que el 'viceversa' del asegura se cumpla
mismosElementos :: (Eq t) => [t] -> [t] -> Bool
mismosElementos xs ys = mismosElementosInterno xs ys && mismosElementosInterno ys xs

-- 2.9
-- Esta función me ayuda a saber si dos secuencias son IGUALES
secuenciasIguales :: (Eq t) => [t] -> [t] -> Bool
secuenciasIguales [] [] = True
secuenciasIguales [] _ = False
secuenciasIguales _ [] = False
secuenciasIguales [x] [y] = x == y
secuenciasIguales (x:xs) (y:ys) | lenXs /= lenYs = False
                                | x == y = secuenciasIguales xs ys
                                | otherwise = False
                                where lenXs = longitud xs
                                      lenYs = longitud ys

capicua :: (Eq t) => [t] -> Bool
capicua xs = secuenciasIguales xs (reverso xs)

-- 3.1
-- redefino sumatoria usando (Num t) para poder hacer el 5.1 sin hacer otra sumatoria
sumatoria :: (Num t) => [t] -> t
sumatoria [] = 0
sumatoria [x] = x
sumatoria (x:xs) = x + sumatoria xs

-- 3.2
productoria :: [Integer] -> Integer
productoria [] = 0
productoria [x] = x
productoria (x:xs) = x * productoria xs

-- 3.3
maximo :: [Integer] -> Integer
maximo [x] = x
maximo (x:xs) | x > maxXs = x
              | otherwise = maxXs
              where maxXs = maximo xs

-- 3.4
-- len res = len s
-- res[i] = n + s[i] para todo 0 <= i < len s <=> len s > 0
sumarN :: Integer -> [Integer] -> [Integer]
sumarN _ [] = []
sumarN n (x:xs) = (x + n) : sumarN n xs

-- 3.5
-- len s > 0
sumarElPrimero :: [Integer] -> [Integer]
sumarElPrimero (x:xs) = sumarN x (x:xs)

-- 3.6
-- len s > 0
sumarElUltimo :: [Integer] -> [Integer]
sumarElUltimo (x:xs) = sumarN (ultimo (x:xs)) (x:xs)

-- 3.7
pares :: [Integer] -> [Integer]
pares [] = []
pares (x:xs) | xEsPar = x : pares xs
             | otherwise = pares xs
             where xEsPar = x `mod` 2 == 0

-- 3.8
multiplosDeN :: Integer -> [Integer] -> [Integer]
multiplosDeN _ [] = []
multiplosDeN n (x:xs) | xEsMultiploDeN = x : multiplosDeN n xs
                      | otherwise = multiplosDeN n xs
                      where xEsMultiploDeN = x `mod` n == 0

-- 3.9
-- se fija si es el numero mas chico de la secuencia
esMenorOIgualATodos :: Integer -> [Integer] -> Bool
esMenorOIgualATodos _ [] = undefined
esMenorOIgualATodos n [v] = n <= v
esMenorOIgualATodos n (x:xs) = n <= x && esMenorOIgualATodos n xs

-- retorna s sin el numero que fue extraido y retorna el numero extraido
sacarNumeroMasChico :: [Integer] -> ([Integer], Integer)
sacarNumeroMasChico [] = undefined
sacarNumeroMasChico [t] = ([], t)
sacarNumeroMasChico (x:xs) | x `esMenorOIgualATodos` xs = (xs, x)
                           | otherwise = (x : fst retorno, snd retorno)
                           where retorno = sacarNumeroMasChico xs

-- se fija si la lista está ordenada
listaEstaOrdenadaCreciente :: [Integer] -> Bool
listaEstaOrdenadaCreciente [] = True
listaEstaOrdenadaCreciente [x] = True
listaEstaOrdenadaCreciente (x:y:xs) = x <= y && listaEstaOrdenadaCreciente (y:xs)

ordenar :: [Integer] -> [Integer]
ordenar [] = []
ordenar [x] = [x]
ordenar xs | listaEstaOrdenadaCreciente xs = xs
           | otherwise = numeroMasChico : ordenar numerosMayores
           where retornoNumeroMasChico = sacarNumeroMasChico xs
                 numeroMasChico = snd retornoNumeroMasChico
                 numerosMayores = fst retornoNumeroMasChico

-- 4: En el ejercicio 4 voy a hacer todo siguiendo las instrucciones del punto b)
-- es decir, voy a resolverlo usando el type Texto
type Texto = [Char];

-- 4.a
sacarBlancosRepetidos :: Texto -> Texto
sacarBlancosRepetidos [] = []
sacarBlancosRepetidos [x] = [x]
sacarBlancosRepetidos (x:y:xs) | x == y && x == ' ' = sacarBlancosRepetidos (y:xs)
                               | otherwise = x : sacarBlancosRepetidos (y:xs)

-- 4.b
sacarUltimoEspacioLateral :: Texto -> Texto
sacarUltimoEspacioLateral [] = []
sacarUltimoEspacioLateral [x] | x == ' ' = []
                              | otherwise = [x]
sacarUltimoEspacioLateral (x:xs) = x : sacarUltimoEspacioLateral xs

sacarPrimerEspacioLateral :: Texto -> Texto
sacarPrimerEspacioLateral [] = []
sacarPrimerEspacioLateral [x] | x == ' ' = []
                              | otherwise = [x]
sacarPrimerEspacioLateral (x:xs) | x == ' ' = xs
                                 | otherwise = x:xs

sacarEspaciosLaterales :: Texto -> Texto
sacarEspaciosLaterales [] = []
sacarEspaciosLaterales [x] = [x]
sacarEspaciosLaterales xs = sacarUltimoEspacioLateral (sacarPrimerEspacioLateral xs)

-- cuenta cuantas veces encuentra n en s
contarElementosN :: (Eq t) => t -> [t] -> Integer
contarElementosN _ [] = 0
contarElementosN n [v] | n == v = 1
                       | otherwise = 0
contarElementosN n (x:xs) | n == x = 1 + contarElementosN n xs
                          | otherwise = contarElementosN n xs

contarPalabras :: Texto -> Integer
contarPalabras [] = 0
contarPalabras [_] = 1
contarPalabras texto = 1 + contarElementosN ' ' (sacarEspaciosLaterales (sacarBlancosRepetidos texto))

-- 4.c
-- sacarPalabra retorna la palabra extraída y el resto del texto
-- requiere: |texto| > 0
sacarPalabra :: Texto -> (Texto, Texto)
sacarPalabra [] = undefined
sacarPalabra [x] = ([x], [])
sacarPalabra (x:xs) | x == ' ' = ([], xs)
                    | otherwise = (x:fst sacarPalabraRec, snd sacarPalabraRec)
                    where sacarPalabraRec = sacarPalabra xs

-- 'palabras' pero sin procesar los espacios repetidos o espacios laterales
-- requiere: ningun espacio repetido y ningun espacio "lateral"
palabrasInterno :: Texto -> [Texto]
palabrasInterno [] = []
palabrasInterno [x] = [[x]]
palabrasInterno xs | longitud xs == 0 = []
                   | otherwise = palabra : palabrasInterno resto
                   where sacarPalabraRetorno = sacarPalabra xs
                         palabra = fst sacarPalabraRetorno
                         resto = snd sacarPalabraRetorno

palabras :: Texto -> [Texto]
palabras xs = palabrasInterno (sacarEspaciosLaterales (sacarBlancosRepetidos xs))

-- 4.d
-- esta función retorna la lista más larga
secuenciaMasLarga :: [[t]] -> [t]
secuenciaMasLarga [] = []
secuenciaMasLarga [v] = v
secuenciaMasLarga (x:y:xss) | longitud x > longitud y = secuenciaMasLarga (x:xss)
                            | otherwise = secuenciaMasLarga (y:xss)

palabraMasLarga :: Texto -> Texto
palabraMasLarga texto = secuenciaMasLarga (palabras texto)

-- 4.e
aplanar :: [Texto] -> Texto
aplanar [] = []
aplanar [x] = x
aplanar (x:xs) = x ++ aplanar xs

-- 4.f y 4.g
-- implementé aplanarConBlancos usando aplanarConNBlancos, n = 1

-- n >= 0
repetirNVeces :: (Integral n) => t -> n -> [t]
repetirNVeces _ 0 = []
repetirNVeces v 1 = [v]
repetirNVeces v n = v : repetirNVeces v (n-1)

-- n >= 0
aplanarConNBlancosConEspacioLateral :: [Texto] -> Integer -> Texto
aplanarConNBlancosConEspacioLateral [] n = []
aplanarConNBlancosConEspacioLateral [x] n = x
aplanarConNBlancosConEspacioLateral (xs:xss) n = xs ++ repetirNVeces ' ' n ++ aplanarConNBlancosConEspacioLateral xss n

-- n >= 0
aplanarConNBlancos :: [Texto] -> Integer -> Texto
aplanarConNBlancos xss n = sacarUltimoEspacioLateral (aplanarConNBlancosConEspacioLateral xss n)

aplanarConBlancos :: [Texto] -> Texto
aplanarConBlancos xss = aplanarConNBlancos xss 1

-- 5.1
-- todos los elementos son >0
-- hago la suma al revés y después revierto la secuencia, es más fácil
sumaAcumuladaReversa :: (Num t) => [t] -> [t]
sumaAcumuladaReversa [] = []
sumaAcumuladaReversa [v] = [v]
sumaAcumuladaReversa (x:xs) = sumatoria (x:xs) : sumaAcumuladaReversa xs

sumaAcumulada :: (Num t) => [t] -> [t]
sumaAcumulada vs = reverso (sumaAcumuladaReversa (reverso vs))

-- 5.2
menorDivisorInterno :: Integer -> Integer -> Integer
menorDivisorInterno n iter | iter < 2 = undefined -- 2 para adelante
                           | n == 1 = 1
                           | iter == n = n
                           | n `mod` iter == 0 = iter
                           | otherwise = menorDivisorInterno n (iter+1)

menorDivisor :: Integer -> Integer
menorDivisor n | n < 1 = undefined
               | otherwise = menorDivisorInterno n 2

esPrimo :: Integer -> Bool
esPrimo n = menorDivisor n == n

siguientePrimo :: Integer -> Integer
siguientePrimo n | esPrimo m = m
                 | otherwise = siguientePrimo m
                 where m = n + 1

-- el primer parámetro es el entero y el segundo es el número primo en uso
descomponerEnteroEnPrimosInterno :: Integer -> Integer -> [Integer]
descomponerEnteroEnPrimosInterno n prim | n == 1 = []
                                        | n `mod` prim == 0 = prim : descomponerEnteroEnPrimosInterno nDivididoPrimo (siguientePrimo prim)
                                        | otherwise = descomponerEnteroEnPrimosInterno n (siguientePrimo prim)
                                        where nDivididoPrimo = n `div` prim

-- todos los 'n' en 's' son mayores a 2
descomponerEnPrimos :: [Integer] -> [[Integer]]
descomponerEnPrimos [] = []
descomponerEnPrimos [v] = [descomponerEnteroEnPrimosInterno v 2]
descomponerEnPrimos (x:xs) = descomponerEnteroEnPrimosInterno x 2 : descomponerEnPrimos xs

main = do

    print (longitud [1, 2, 3, 4]) -- 4
    print (longitud [1..10]) -- 10
    print (longitud [1..9999]) -- 9999
    print (longitud []) -- 0

    print (ultimo [1, 2, 3]) -- 3
    print (ultimo [1..10]) -- 10
    print (ultimo [20, 52, 12]) -- 12

    print (principio [1, 2, 3]) -- [1, 2]
    print (principio [1..10]) -- [1, 2, 3, 4, 5, 6, 7, 8, 9]

    print (reverso [1]) -- [1]
    print (reverso [1, 2]) -- [2, 1]
    print (reverso [1..5]) -- [5, 4, 3, 2, 1]

    print (pertenece 10 [1..8]) -- False
    print (pertenece 10 [1..16]) -- True
    print (pertenece 'a' ['h', 'o', 'l', 'a']) -- True

    print (todosIguales [1, 2, 2]) -- False
    print (todosIguales [1, 2]) -- False
    print (todosIguales [2, 1]) -- False
    print (todosIguales [2, 1, 1]) -- False
    print (todosIguales [1, 1, 1, 2, 1, 1, 1]) -- False
    print (todosIguales [2, 2, 2]) -- True
    print (todosIguales [1, 1]) -- True
    print (todosIguales [1, 1, 1]) -- True
    print (todosIguales [1, 1, 1, 1, 1, 1, 1]) -- True
    print (todosIguales [7]) -- True
    print (todosIguales [7, 7]) -- True

    print (todosDistintos [1..10]) -- True
    print (todosDistintos [1, 2, 2]) -- False
    print (todosDistintos [1, 2]) -- True
    print (todosDistintos [2, 1]) -- True
    print (todosDistintos [2, 1, 1]) -- False
    print (todosDistintos [1, 1, 1, 2, 1, 1, 1]) -- False
    print (todosDistintos [2, 2, 2]) -- False
    print (todosDistintos [1, 1]) -- False
    print (todosDistintos [2, 1, 2]) -- False

    print (hayRepetidos [1, 2, 3, 4, 5]) -- False
    print (hayRepetidos [1, 2, 3, 4, 5, 1]) -- True
    print (hayRepetidos ([1..100] ++ [200])) -- False
    print (hayRepetidos ([1..100] ++ [50])) -- True

    print (quitar 7 [1..10]) -- [1, 2, 3, 4, 5, 6, 8, 9, 10]
    print (quitar 3 ([1..5] ++ [3, 3, 3])) -- [1, 2, 4, 5, 3, 3, 3]

    print (quitar 7 [1..10]) -- [1, 2, 3, 4, 5, 6, 8, 9, 10]
    print (quitarTodos 3 ([1..5] ++ [3, 3, 3])) -- [1, 2, 4, 5]

    print (eliminarRepetidos [1, 2, 3, 4]) -- [1, 2, 3, 4]
    print (eliminarRepetidos [1, 2, 3, 4, 2, 3]) -- [1, 2, 3, 4]
    print (eliminarRepetidos ([1..5] ++ [3, 3, 3])) -- [1, 2, 3, 4, 5]

    print (mismosElementosInterno [1, 2, 3, 4] [1, 1, 1, 2, 3, 2, 2, 6, 5, 4, 4]) -- True
    print (mismosElementosInterno [1, 1, 1, 2, 3, 2, 2, 6, 5, 4, 4] [1, 2, 3, 4]) -- False

    print (mismosElementos [1, 2, 3, 4] [1, 1, 1, 2, 3, 2, 2, 6, 5, 4, 4]) -- False
    print (mismosElementos [1, 1, 1, 2, 3, 2, 2, 6, 5, 4, 4] [1, 2, 3, 4]) -- False
    print (mismosElementos [1..9] ([4..9] ++ [1..3])) -- True
    print (mismosElementos [7, 3, 5, 9, 1, 3, 3, 7, 7, 5, 9] [3, 5, 7, 9]) -- False
    print (mismosElementos [7, 3, 5, 9, 1, 3, 3, 7, 7, 5, 9] [1, 3, 5, 7, 9]) -- True

    print (secuenciasIguales [1, 2, 3, 4] [1, 2, 3, 4]) -- True
    print (secuenciasIguales [1, 2, 3, 4] [1, 2, 4, 3]) -- False
    print (secuenciasIguales [1, 2, 3] [1, 2]) -- False
    print (secuenciasIguales [1, 2] [1, 2, 3]) -- False
    print (secuenciasIguales [] [1]) -- False
    print (secuenciasIguales [1] []) -- False

    print (capicua "hola") -- False
    print (capicua "neuquen") -- True
    print (capicua "acbbca") -- True

    print (sumatoria [1, 2, 3, 4, 5]) -- 15
    print (sumatoria []) -- 0
    print (sumatoria [3, 3, 3, 3, 3]) -- 15
    print (sumatoria [27, 32, 11, 2]) -- 72
    print (sumatoria [1..100]) -- 5050

    print (productoria [1..5]) -- 5! = 120
    print (productoria ([3..5] ++ [11..12])) -- 7920
    print (productoria []) -- 0
    print (productoria [10]) -- 10

    print (maximo [1]) -- 1
    print (maximo [1..10]) -- 10
    print (maximo ([72..99] ++ [1..50])) -- 99

    print (sumarN 10 [1, 2, 3]) -- [11, 12, 13]
    print (sumarN 999 [1..100]) -- [1000, 1001, ..., 1099]

    print (sumarElPrimero [1..10]) -- [2..11]
    print (sumarElPrimero [10..15]) -- [20..25]

    print (sumarElUltimo [1..10]) -- [11..20]
    print (sumarElUltimo [10..15]) -- [25..30]

    print (pares [1..20]) -- [1,2..20]
    print (pares ([50..80] ++ [99..121])) -- [50,2..80] ++ [100,2..120]

    print (multiplosDeN 2 [1..20]) -- [1,2..20]
    print (multiplosDeN 3 [1..20]) -- [3,6..18]
    print (multiplosDeN 3 [-20..20]) -- [-18,-15..18]

    print (esMenorOIgualATodos 1 [1..20]) -- True
    print (esMenorOIgualATodos 5 [1..10]) -- False
    print (esMenorOIgualATodos (-10) [1..20]) -- True

    print (sacarNumeroMasChico ([20..30] ++ [10..15])) -- saca el 10
    print (sacarNumeroMasChico [7, 6, 5, 4, 3, 2, 1]) -- saca el 1

    print (listaEstaOrdenadaCreciente [1..10]) -- True
    print (listaEstaOrdenadaCreciente [1,1,1,1,1,1,1,1,2,2,2,2,2,3,4,5,6]) -- True
    print (listaEstaOrdenadaCreciente [1,2,3,4,5,6,5]) -- False

    print (ordenar [1..10]) -- [1..10]
    print (ordenar [7, 6, 5, 4, 3, 2, 1]) -- [1..7]
    print (ordenar ([1..10] ++ [5,7..92] ++ [-15,-11..13]))

    print (sacarBlancosRepetidos "hola buenos dias") -- "hola buenos dias"
    print (sacarBlancosRepetidos "hola     buenos    dias") -- "hola buenos dias"
    print (sacarBlancosRepetidos " hola     buenos    dias  ") -- " hola buenos dias "

    print (sacarEspaciosLaterales "hola") -- "hola"
    print (sacarPrimerEspacioLateral (sacarBlancosRepetidos " hola     buenos    dias  ")) -- "hola buenos dias "
    print (sacarUltimoEspacioLateral (sacarBlancosRepetidos " hola     buenos    dias  ")) -- " hola buenos dias"
    print (sacarEspaciosLaterales (sacarBlancosRepetidos " hola     buenos    dias  ")) -- "hola buenos dias"

    print (contarElementosN ' ' "hola buenos dias") -- 2
    print (contarPalabras "hola buenos dias, que tal?") -- 5
    print (contarPalabras "  hola q   estas  haciendo   ") -- 4

    print (sacarPalabra "hola buenas") -- "hola"
    
    print (palabras "hola que tal")
    print (palabras " holaaaa que tal   todo bien???  espectacular! ")
    print (palabras "")

    print (secuenciaMasLarga [[] :: Texto]) -- []
    print (secuenciaMasLarga ["hola", "que", "tal", "todobien"]) -- "todobien"
    print (palabraMasLarga "hola que tal, todo bien? me alegro!") -- "alegro!"
    print (aplanar ["holaaaa", "que", "tal"]) -- "holaaaaquetal"

    print (aplanarConBlancos ["holaaaa", "que", "tal"]) -- "holaaaa que tal"
    print (aplanarConNBlancos ["holaaaa", "que", "tal"] 2) -- "holaaaa  que  tal"

    print (sumaAcumuladaReversa [5, 4, 3, 2, 1]) -- [15, 10, 6, 3, 1]
    print (sumaAcumulada [1, 2, 3, 4, 5]) -- [1, 3, 6, 10, 15]
    print (sumaAcumulada [0,4..20]) -- [0, 4, 12, 24, 40, 60]

    print (descomponerEnteroEnPrimosInterno 214 2) -- [2, 107]
    print (descomponerEnPrimos [2, 10, 6]) -- [[2], [2, 5], [2, 3]]
